package com.kfcwebstandard.servlets;

import com.kfwebstandard.jpacontroller.FishActionBeanJPA;
import com.kfwebstandard.entities.Fish;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet class that displays the contents of the Aquarium database as a simple
 * HTML table using the JPA
 *
 * @author Ken Fogel
 */
@WebServlet(name = "JPAServlet", urlPatterns = {"/JPAServlet"})
public class JPAServlet extends HttpServlet {

    private final static Logger LOG = LoggerFactory.getLogger(JPAServlet.class);

    @Inject
    private FishActionBeanJPA fab;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Fish> fishies = null;

        try {
            fishies = fab.getAll();
        } catch (SQLException ex) {
            LOG.error("Error in doGet calling fab.getAll()", ex);
        }

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println(
                    "<!doctype html>\n"
                    + "<html>\n"
                    + "<head>\n"
                    + "<title>JPA and Servlet</title>\n"
                    + "</head>\n");
            out.println("<p><TABLE RULES=all>");
            if (fishies != null) {
                for (Fish f : fishies) {
                    out.println("<tr>");
                    out.print("<td>"
                            + f.getId()
                            + "</td><td>"
                            + f.getCommonname()
                            + "</td><td>"
                            + f.getLatin()
                            + "</td><td>"
                            + f.getPh()
                            + "</td><td>"
                            + f.getKh()
                            + "</td><td>"
                            + f.getTemp()
                            + "</td><td>"
                            + f.getFishsize()
                            + "</td><td>"
                            + f.getSpeciesorigin()
                            + "</td><td>"
                            + f.getTanksize()
                            + "</td><td>"
                            + f.getStocking()
                            + "</td><td>"
                            + f.getDiet()
                            + "</td>");
                    out.println("</tr>");
                }
            }
            out.println("</table></body></html>");
        }
    }
}

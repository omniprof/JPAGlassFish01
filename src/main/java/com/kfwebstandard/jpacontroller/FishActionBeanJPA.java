package com.kfwebstandard.jpacontroller;

import com.kfwebstandard.entities.Fish;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A magic bean that returns a list of fish
 *
 * @author Ken
 *
 */
@Named
@RequestScoped
public class FishActionBeanJPA implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(FishActionBeanJPA.class);

    @PersistenceContext(unitName = "fishiesPU")
    private EntityManager entityManager;

    public List<Fish> getAll() throws SQLException {

        // Object oriented criteria builder
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Fish> cq = cb.createQuery(Fish.class);
        Root<Fish> fish = cq.from(Fish.class);
        cq.select(fish);
        TypedQuery<Fish> query = entityManager.createQuery(cq);

        // Using a named query
//        TypedQuery<Fish> query =  entityManager.createNamedQuery("Fish.findAll", Fish.class);
        // Execute the query
        List<Fish> fishies = query.getResultList();

        return fishies;
    }
}
